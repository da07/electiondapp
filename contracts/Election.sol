pragma solidity ^0.4.2;

contract Election {
    // model a candidate
    struct Candidate {
        uint id;
        string name;
        uint voteCount;
    }

    //store accounts that have voted
    mapping(address => bool) public voters;
    // store candidates : map candidates to an array
    //fetch candidates
    mapping(uint => Candidate) public candidates;
    //store candidates count
    uint public candidatesCount;

    // voted event
    event votedEvent(
        uint indexed _candidateId
    );
    // constructor
    constructor() public {
        addCandidate("Candidate 1");
        addCandidate("Candidate 2");
    }
    // local variable starts with _
    function addCandidate (string _name) private {
        candidatesCount ++;
        candidates[candidatesCount] = Candidate(candidatesCount, _name, 0);
    }
    function vote (uint _candidateId) public {
        //require that they have't voted befoe
        require(
            !voters[msg.sender],
            "Requiring a vergin voter XD");
        //require a valid candidate
        require(
            _candidateId > 0 && _candidateId <= candidatesCount,
            "Requiring valid candidate"
            );
        //record that voter has voted
        voters[msg.sender] = true;
        //update candidate vote count
        candidates[_candidateId].voteCount ++;
        //TRIGGER EVENT
        emit votedEvent(_candidateId);
    }
}